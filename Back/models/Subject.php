<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Subject".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $create_date
 * @property int|null $id_teacher
 *  @property int|null $enabled
 *
 * @property Teacher $teacher
 * @property SudentDetailsSubject[] $sudentDetailsSubjects
 * @property Student[] $students
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['id_teacher', 'enabled'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['id_teacher'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['id_teacher' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'id_teacher' => 'Id Teacher',
            'enabled' => 'Enabled'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'id_teacher'])->select("id,name,last_name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSudentDetailsSubjects()
    {
        return $this->hasMany(Sudent_details_subject::className(), ['id_subject' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['id' => 'id_student'])->viaTable('Sudent_details_subject', ['id_subject' => 'id'])->select("id,name,last_name");
    }
}
