<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Teacher".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $last_name
 * @property string|null $create_date
 * @property string|null $phone
 * @property int|null $enabled
 *
 * @property Subject[] $subjects
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Teacher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['phone'], 'string', 'max' => 10],
            [['name', 'last_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
            'phone' => 'Phone'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['id_teacher' => 'id']);
    }
}
