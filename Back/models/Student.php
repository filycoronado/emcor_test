<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Student".
 *
 * @property int $id
 * @property string|null $name
 * @property string $last_name
 * @property string|null $create_date
 * @property int|null $enabled
 * @property string|null $phone
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['last_name'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['phone'], 'string', 'max' => 10],
            [['name', 'last_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
            'phone' => 'Phone'
        ];
    }
}
