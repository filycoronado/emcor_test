<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Sudent_details_subject".
 *
 * @property int $id_subject
 * @property int $id_student
 * @property string|null $create_date
 * @property string|null $enabled
 *
 * @property Subject $subject
 * @property Student $student
 */
class Sudent_details_subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Sudent_details_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_subject', 'id_student'], 'required'],
            [['id_subject', 'id_student'], 'integer'],
            [['create_date'], 'safe'],
            [['enabled'], 'string', 'max' => 10],
            [['id_student', 'id_subject'], 'unique', 'targetAttribute' => ['id_student', 'id_subject']],
            [['id_subject'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['id_subject' => 'id']],
            [['id_student'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['id_student' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_subject' => 'Id Subject',
            'id_student' => 'Id Student',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'id_subject']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'id_student']);
    }
}
