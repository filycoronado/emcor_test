<?php

namespace app\controllers;

use app\models\Subject;
use app\models\Sudent_details_subject;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class Subject_studentController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }





    public function actionAssing_student()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $subject = new Sudent_details_subject();
        $subject->attributes = $data;
        $subject->enabled = 1;
        $subject->create_date = date("Y-m-d");
        $item = Sudent_details_subject::find()->where("id_subject=" . $subject->id_subject)->andWhere("id_student=" . $subject->id_student)->one();
        $response = [
            "status" => "error",
            "message" => "Error Al Crear"
        ];

        if ($item == null) {
            if ($subject->save(false)) {
                $response = [
                    "status" => "success",
                    "message" => "Asigando  Con Exito",
                    "data" => $subject
                ];
            }
        } else {
            $response = [
                "status" => "error",
                "message" => "Este estudainte ya esta agregado a este curso."
            ];
        }
        return $response;
    }

    public function actionDetails()
    {

        $items = Subject::find()
        ->where("enabled=1")
        ->with("teacher")
        ->with("students")
        ->asArray()
        ->all();

        $response = [
            "status" => "error",
            "message" => "No hay registros"
        ];
        if ($items != null) {
            $response = [
                "status" => "success",
                "message" => "Registros Encontrados",
                "data" => $items
            ];
        }
        return $response;
    }
}
