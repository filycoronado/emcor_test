<?php

namespace app\controllers;




use app\models\Student;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class StudentController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }






    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $teaher = new Student();
        $teaher->attributes = $data;
        $teaher->enabled = 1;
        $teaher->create_date = date("Y-m-d");
        $response = [
            "status" => "error",
            "message" => "Error Al Crear"
        ];
        if ($teaher->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Estudiante Creado Con Exito"
            ];
        }
        return $response;
    }

    public function actionUpdate()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $teaher = Student::findOne($id);
        $teaher->attributes = $data;

        $response = [
            "status" => "error",
            "message" => "Error  Actualizar"
        ];
        if ($teaher->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Estudiante Actualizado Con Exito"
            ];
        }
        return $response;
    }

    public function actionDelete($id)
    {
        $teaher = Student::findOne($id);
        $teaher->enabled = 0;

        $response = [
            "status" => "error",
            "message" => "Error Eliminar"
        ];
        if ($teaher->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Estudiante Eliminado Con Exito"
            ];
        }
        return $response;
    }

    public function actionGet_all()
    {
        $list = Student::find()->where("enabled=1")->all();
        $response = [
            "status" => "error",
            "message" => "No hay registros"
        ];
        if ($list != null) {
            $response = [
                "status" => "success",
                "message" => "Registros Encontrados",
                "data" => $list
            ];
        }

        return $response;
    }

    public function actionGet_teacher($id)
    {

        $item = Student::findOne($id);
        $response = [
            "status" => "error",
            "message" => "No hay registros"
        ];
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Registros Encontrados",
                "data" => $item
            ];
        }

        return $response;
    }
}
