<?php

namespace app\controllers;




use app\models\Subject;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class SubjectController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }






    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $subject = new Subject();
        $subject->attributes = $data;
        $subject->enabled=1;
        $subject->id_teacher = NULL;
        $subject->create_date = date("Y-m-d");
        $response = [
            "status" => "error",
            "message" => "Error Al Crear"
        ];
        if ($subject->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Materia Creado Con Exito",
                "data" => $subject
            ];
        }
        return $response;
    }

    public function actionUpdate()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $subject = Subject::findOne($id);
        $subject->attributes = $data;

        $response = [
            "status" => "error",
            "message" => "Error  Actualizar"
        ];
        if ($subject->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Materia Actualizado Con Exito"
            ];
        }
        return $response;
    }

    public function actionDelete($id)
    {
        $subject = Subject::findOne($id);
        $subject->enabled = 0;

        $response = [
            "status" => "error",
            "message" => "Error Eliminar"
        ];
        if ($subject->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Materia Eliminado Con Exito"
            ];
        }
        return $response;
    }

    public function actionGet_all()
    {
        $list = Subject::find()->where("enabled=1")->all();
        $response = [
            "status" => "error",
            "message" => "No hay registros"
        ];
        if ($list != null) {
            $response = [
                "status" => "success",
                "message" => "Registros Encontrados",
                "data" => $list
            ];
        }

        return $response;
    }

    public function actionGet_teacher($id)
    {

        $item = Subject::findOne($id);
        $response = [
            "status" => "error",
            "message" => "No hay registros"
        ];
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Registros Encontrados",
                "data" => $item
            ];
        }

        return $response;
    }

    public function actionAssig_teacher()
    {
    }
}
