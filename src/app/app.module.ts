import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from '../components/components.module';
import { HomeComponentComponent } from 'src/Pages/home-component/home-component.component';
import { NgxSpinnerModule } from "ngx-spinner";

//Translation
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TeacherListComponent } from 'src/components/Teacher/teacher-list/teacher-list.component';
import { TeacherFormComponent } from 'src/components/Teacher/teacher-form/teacher-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentListComponent } from 'src/components/Students/Student-list/Student-list.component';
import { StudentFormComponent } from 'src/components/Students/Student-form/Student-form.component'; 
import { SubjectListComponent } from 'src/components/Subject/subject-list/subject-list.component';
import { SubjectFormComponent } from 'src/components/Subject/subject-form/subject-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    TeacherListComponent,
    TeacherFormComponent,
    StudentListComponent,
    StudentFormComponent,
    SubjectListComponent,
    SubjectFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
