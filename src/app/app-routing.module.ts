import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponentComponent } from 'src/Pages/home-component/home-component.component';
import { TranslateModule } from '@ngx-translate/core';
import { TeacherListComponent } from 'src/components/Teacher/teacher-list/teacher-list.component';
import { TeacherFormComponent } from 'src/components/Teacher/teacher-form/teacher-form.component';
import { StudentListComponent } from 'src/components/Students/Student-list/Student-list.component';
import { StudentFormComponent } from 'src/components/Students/Student-form/Student-form.component';
import { SubjectListComponent } from 'src/components/Subject/subject-list/subject-list.component';
import { SubjectFormComponent } from 'src/components/Subject/subject-form/subject-form.component';


const routes: Routes = [
  { path: 'Home', component: HomeComponentComponent },
  { path: '', component: HomeComponentComponent },
  { path: 'Maestros', component: TeacherListComponent },
  { path: 'Maestros/Form', component: TeacherFormComponent },
  { path: 'Maestros/Form/:id', component: TeacherFormComponent },
  { path: 'Estudiantes', component: StudentListComponent },
  { path: 'Estudiantes/Form', component: StudentFormComponent },
  { path: 'Estudiantes/Form/:id', component: StudentFormComponent },
  { path: 'Materias', component: SubjectListComponent },
  { path: 'Materias/Form', component: SubjectFormComponent },
  { path: 'Materias/Form/:id', component: SubjectFormComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule, TranslateModule]
})
export class AppRoutingModule {

}
