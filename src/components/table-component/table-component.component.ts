import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.css']
})
export class TableComponentComponent implements OnInit {
  @Input() teachers: any[];
  @Input() colums: any[];
  @Input() url_edit: string;
  @Output() valueChange = new EventEmitter();
  @Input() mode_asignation: boolean;
  constructor() { }

  ngOnInit() {
    // console.log(this.teachers.length);
  }
  valueChanged(id) {
    this.valueChange.emit(id);
  }
}
