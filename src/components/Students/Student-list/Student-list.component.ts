import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
// jQuery
import $ from 'jquery';
import { ApiService } from 'src/components/Services/ApiService';
@Component({
  selector: 'app-Student-list',
  templateUrl: './Student-list.component.html',
  styleUrls: ['./Student-list.component.scss']
})
export class StudentListComponent implements OnInit {

  teachers;
  colums = ["Nombre", "Apellido", "Telefono", "Fecha"];
  constructor(private spinner: NgxSpinnerService, private apiservice: ApiService) { }

  ngOnInit() {

    // show spinner
    this.spinner.show();
    this.apiservice.getAllStudent().then(res => {
      if (res.status == "success") {
        this.teachers = res.data;

      } else {
        alert(res.message);
      }
      this.spinner.hide();

    });
    // init scrollable menu
    this.initMenuScrollable();
  }

  initMenuScrollable() {
    $(function () {
      /* $("html, body").animate(
        {
          scrollTop: 0
        },
        1000
      ); */
      let scroll = window.scrollY;
      if (scroll > 0) {
        $('#menu').addClass('menu-sm');
      } else {
        $('#menu').removeClass('menu-sm');
      }
      function scrollFunction() {
        //console.log('asda');
        scroll = window.scrollY;
        //console.log(scroll);
        if (scroll > 0) {
          $('#menu').addClass('menu-sm');
        } else {
          $('#menu').removeClass('menu-sm');
        }
      }
      window.onscroll = scrollFunction;
      $('.menu-link, .nav-link').click(function () {
        $('.nav-item').removeClass('active');
        $(this).parent().addClass('active');
        if ($(this).attr('href') == '#home') {
          console.log('home');
          $("html, body").animate(
            {
              scrollTop: 0
            },
            1000
          );
          return;
        }
        var target = $(this.hash);
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000
          );
        }
      });
    });
  }
  ActionDelete(id): void {
    if (confirm("Deseas Eliminar Este Elemento? ")) {
      this.spinner.show();
      this.apiservice.deleteStudent(id).then(res => {
        if (res.status == "success") {
          location.reload();
          
        }
        
        this.spinner.hide();
      });
    }

  }

}
