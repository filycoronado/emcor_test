import { Injectable } from '@angular/core';
import { GlobalService } from './Global.Service';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(public globalService: GlobalService, private http: HttpClient) {

    }
    /**Method Save Teacher */
    saveTeacher(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'teacher/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Update Teacher */
    updateTeacher(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'teacher/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Get All Teeacher Avaliables */
    getAllTeachers(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'teacher/get_all', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Get By Id Teacher */
    getTeacherById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'teacher/get_teacher&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Delete (logical) */
    deleteTeacher(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'teacher/delete&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }



    saveStudent(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'student/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }


    /**Method Update Student */
    updateStudent(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'student/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Get All Student Avaliables */
    getAllStudent(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'student/get_all', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Get By Id Student */
    getStudentById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'student/get_teacher&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    /**Method Delete (logical) */
    deleteStudent(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'student/delete&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }



    /**Method Save Teacher */
    saveSubject(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'subject/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    updateSubject(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'subject/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    assigStudent(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'subject_student/assing_student', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    getSubjectDetails(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'subject_student/details', { }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

}