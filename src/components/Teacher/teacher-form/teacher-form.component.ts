import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
// jQuery
import $ from 'jquery';
import { ApiService } from 'src/components/Services/ApiService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-teacher-form',
  templateUrl: './teacher-form.component.html',
  styleUrls: ['./teacher-form.component.scss']
})
export class TeacherFormComponent implements OnInit {
  public data = {
    name: "",
    last_name: "",
    phone: ""
  };
  id;
  edit;
  title;
  public registerTeacherForm: FormGroup;
  submited = false;
  constructor(private spinner: NgxSpinnerService, private apiservice: ApiService, private formBuilder: FormBuilder, private _location: Location, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {

      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.apiservice.getTeacherById(this.id).then(res => {
          if (res.status == "success") {
            this.data = res.data;
            this.setDataForm();
            this.edit = true;
            this.title = "Editar";
          }
          this.spinner.hide();
        });
      }

    });



    this.initForm();


    // init scrollable menu
    this.initMenuScrollable();
  }

  initMenuScrollable() {
    $(function () {
      /* $("html, body").animate(
        {
          scrollTop: 0
        },
        1000
      ); */
      let scroll = window.scrollY;
      if (scroll > 0) {
        $('#menu').addClass('menu-sm');
      } else {
        $('#menu').removeClass('menu-sm');
      }
      function scrollFunction() {
        //console.log('asda');
        scroll = window.scrollY;
        //console.log(scroll);
        if (scroll > 0) {
          $('#menu').addClass('menu-sm');
        } else {
          $('#menu').removeClass('menu-sm');
        }
      }
      window.onscroll = scrollFunction;
      $('.menu-link, .nav-link').click(function () {
        $('.nav-item').removeClass('active');
        $(this).parent().addClass('active');
        if ($(this).attr('href') == '#home') {
          console.log('home');
          $("html, body").animate(
            {
              scrollTop: 0
            },
            1000
          );
          return;
        }
        var target = $(this.hash);
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000
          );
        }
      });
    });
  }
  onSubmit(): void {
    if (this.edit) {
      this.update();
    } else {
      this.save();
    }

  }

  save(): void {
    this.getDataForm();
    this.submited = true;
    // stop here if form is invalid
    if (this.registerTeacherForm.invalid) {
      return;
    }

    this.spinner.show();
    this.apiservice.saveTeacher(this.data).then(res => {

      this.spinner.hide();
      alert(res.message);
      if (res.status == "success") {
        this._location.back();
      }

    });
  }
  update(): void {
    this.getDataForm();
    this.submited = true;
    // stop here if form is invalid
    if (this.registerTeacherForm.invalid) {
      return;
    }
    this.spinner.show();
    this.apiservice.updateTeacher(this.data, this.id).then(res => {
      this.spinner.hide();
      alert(res.message);
      if (res.status == "success") {
        this._location.back();
      }
    });
  }

  initForm(): void {
    this.registerTeacherForm = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: [''],
    }, {
    });
  }
  getDataForm(): void {
    this.data.name = this.registerTeacherForm.get("name").value;
    this.data.last_name = this.registerTeacherForm.get("last_name").value;
    this.data.phone = this.registerTeacherForm.get("phone").value;
  }
  setDataForm(): void {
    this.registerTeacherForm.get("name").setValue(this.data.name);
    this.registerTeacherForm.get("last_name").setValue(this.data.last_name);
    this.registerTeacherForm.get("phone").setValue(this.data.phone);
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerTeacherForm.controls; }
}
