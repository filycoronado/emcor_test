import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
// jQuery
import $ from 'jquery';

import { ActivatedRoute } from '@angular/router';

import { ApiService } from 'src/components/Services/ApiService';

@Component({
  selector: 'app-subject-form',
  templateUrl: './subject-form.component.html',
  styleUrls: ['./subject-form.component.scss']
})
export class SubjectFormComponent implements OnInit {

  flag1 = true;
  flag2 = false;
  flag3 = false;
  public data = {
    id: "",
    name: "",
    id_teacher: "",
  };

  public assig = {
    id_subject: "",
    id_student: "",
  }


  teachers;
  students;
  id;
  edit = false;
  title;
  public registerTeacherForm: FormGroup;
  submited = false;

  colums = ["Nombre", "Apellido", "Telefono", "Fecha"];
  constructor(private spinner: NgxSpinnerService, private apiservice: ApiService, private formBuilder: FormBuilder, private _location: Location, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {

      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.apiservice.getTeacherById(this.id).then(res => {
          if (res.status == "success") {
            this.data = res.data;
            this.setDataForm();
            this.edit = true;
            this.title = "Editar";
          }
          this.spinner.hide();
        });
      }

    });

    // show spinner
    this.spinner.show();
    this.apiservice.getAllTeachers().then(res => {
      if (res.status == "success") {
        this.teachers = res.data;
      } else {
        alert(res.message);
      }
      this.spinner.hide();
    });

    this.spinner.show();
    this.apiservice.getAllStudent().then(res => {
      if (res.status == "success") {
        this.students = res.data;

      } else {
        alert(res.message);
      }
      this.spinner.hide();

    });





    this.initForm();


    // init scrollable menu
    this.initMenuScrollable();
  }

  initMenuScrollable() {
    $(function () {
      /* $("html, body").animate(
        {
          scrollTop: 0
        },
        1000
      ); */
      let scroll = window.scrollY;
      if (scroll > 0) {
        $('#menu').addClass('menu-sm');
      } else {
        $('#menu').removeClass('menu-sm');
      }
      function scrollFunction() {
        //console.log('asda');
        scroll = window.scrollY;
        //console.log(scroll);
        if (scroll > 0) {
          $('#menu').addClass('menu-sm');
        } else {
          $('#menu').removeClass('menu-sm');
        }
      }
      window.onscroll = scrollFunction;
      $('.menu-link, .nav-link').click(function () {
        $('.nav-item').removeClass('active');
        $(this).parent().addClass('active');
        if ($(this).attr('href') == '#home') {
          console.log('home');
          $("html, body").animate(
            {
              scrollTop: 0
            },
            1000
          );
          return;
        }
        var target = $(this.hash);
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000
          );
        }
      });
    });
  }
  onSubmit(): void {

    if (this.edit == true) {

      this.update();
    } else {
      console.log("save");
      this.save();
    }
  }

  save(): void {
  
    this.getDataForm();
    this.submited = true;
    // stop here if form is invalid
    if (this.registerTeacherForm.invalid) {
      
      return;
    }

    this.spinner.show();
    this.apiservice.saveSubject(this.data).then(res => {

      this.spinner.hide();
      alert(res.message);
      if (res.status == "success") {
        this.data = res.data;
        $(".btn-frm").prop('disabled', true);
        $(".sec2").prop('disabled', false);
        $(".sec2").click();
      }

    });
  }
  update(): void {
    this.getDataForm();
    this.submited = true;
    // stop here if form is invalid
    if (this.registerTeacherForm.invalid) {
      return;
    }
    this.spinner.show();
    this.apiservice.updateTeacher(this.data, this.id).then(res => {
      this.spinner.hide();
      alert(res.message);
      if (res.status == "success") {
       // this._location.back();
      }
    });
  }

  initForm(): void {
    this.registerTeacherForm = this.formBuilder.group({
      name: ['', Validators.required],
  
    }, {
    });
  }
  getDataForm(): void {
    this.data.name = this.registerTeacherForm.get("name").value;

  }
  setDataForm(): void {
    this.registerTeacherForm.get("name").setValue(this.data.name);
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerTeacherForm.controls; }

  ActionUpdate(id): void {
    if (confirm("Deseas Asignar Este Elemento? ")) {


      this.data.id_teacher = id;
      this.spinner.show();

      this.apiservice.updateSubject(this.data, this.data.id).then(res => {
        alert(res.message);
        if (res.status == "success") {
          $(".sec3").prop('disabled', false);
          $(".sec3").click();
        }
        this.spinner.hide();
      });
    }

  }

  ActionUpdateStaudent(id): void {
    if (confirm("Deseas Asignar Este Elemento? ")) {
      this.assig.id_subject = this.data.id;
      this.assig.id_student = id;
      this.spinner.show();
      this.apiservice.assigStudent(this.assig, this.data.id).then(res => {
        alert(res.message);
        if (res.status == "success") {
          //$(".sec3").prop('disabled', false);
         // $(".sec3").click();
        }
        this.spinner.hide();
      });
    }
  }

}
